package com.quovadis.lamda.model;

public enum Category {
    FOOD,
    UTENSILS,
    CLEANING,
    OFFICE
}
