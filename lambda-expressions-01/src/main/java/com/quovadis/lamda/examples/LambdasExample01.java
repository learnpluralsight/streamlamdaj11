package com.quovadis.lamda.examples;

import com.quovadis.lamda.model.ExampleData;
import com.quovadis.lamda.model.Product;

import java.util.Comparator;
import java.util.List;

public class LambdasExample01 {

    public static void main(String[] args) {
        List<Product> products = ExampleData.getProducts();

        products.sort((p1, p2) -> p1.getPrice().compareTo(p2.getPrice()));

        for (Product product: products) {
            System.out.println(product);
        }
    }
}
