package com.quovadis.functionalinterfaces.model;

public enum Category {
    FOOD,
    UTENSILS,
    CLEANING,
    OFFICE
}
