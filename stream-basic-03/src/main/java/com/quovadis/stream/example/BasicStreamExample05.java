package com.quovadis.stream.example;

import com.quovadis.stream.model.Category;
import com.quovadis.stream.model.ExampleData;
import com.quovadis.stream.model.Product;

import java.util.List;
import java.util.stream.Collectors;

public class BasicStreamExample05 {

    public static void main(String[] args) {
        List<Product> products = ExampleData.getProducts();

        List<String> foodProductNames = products.stream()
                .filter(product -> product.getCategory() == Category.FOOD)
                .map(Product::getName)
                .collect(Collectors.toList());

        System.out.println(foodProductNames);

        String categories = products.stream()
                .map(Product::getCategory)
                .distinct()
                .map(Category::name)
                .collect(Collectors.joining("; "));

        System.out.println(categories);

    }
}
