package com.quovadis.stream.model;

public enum Category {
    FOOD,
    UTENSILS,
    CLEANING,
    OFFICE
}
